<?php


namespace App\Factories;

use App\Exporters\JSONExporter;
use App\Exporters\TxtExporter;
use App\Interfaces\BookInterface;
use App\Interfaces\ExporterFactoryInterface;
use App\Interfaces\ExporterInterface;
use App\Interfaces\PersonInterface;

class ExporterFactory implements ExporterFactoryInterface
{
    private $personEntity;
    private $bookEntity;

    public function __construct(BookInterface $bookEntity, PersonInterface $personEntity)
    {
        $this->personEntity = $personEntity;
        $this->bookEntity = $bookEntity;
    }

    public function createExporter($type): ExporterInterface
    {
        switch ($type) {
            case 'json':
                return new JSONExporter($this->bookEntity, $this->personEntity);
            case 'txt':
                return new TxtExporter($this->bookEntity, $this->personEntity);
            default:
                throw new \Exception("Exporter type {$type} not found");
                break;
        }
    }
}
