<?php


namespace App\Factories;

use App\Entities\PersonEntity;
use App\Importers\CSVImporter;
use App\Importers\XMLImporter;
use App\Interfaces\BookInterface;
use App\Interfaces\ImporterFactoryInterface;
use App\Interfaces\ImporterInterface;

class ImporterFactory implements ImporterFactoryInterface
{
    protected $bookEntity;
    protected $personEntity;

    public function __construct(BookInterface $bookEntity, PersonEntity $personEntity)
    {
        $this->bookEntity = $bookEntity;
        $this->personEntity = $personEntity;
    }

    public function createImporter($type): ImporterInterface
    {
        switch ($type) {
            case 'xml':
                return new XMLImporter($this->bookEntity, $this->personEntity);
            case 'csv':
                return new CSVImporter($this->bookEntity, $this->personEntity);
            default:
                throw new \Exception("Importer type {$type} not found");
                break;
        }
    }
}
