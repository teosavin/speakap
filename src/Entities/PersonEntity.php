<?php


namespace App\Entities;

use App\Interfaces\PersonInterface;

class PersonEntity implements PersonInterface
{

    private $tracker;
    private static $totalCheckedOut = 'totalCheckedOut.txt';
    private static $currentlyCheckedOut = 'currentlyCheckedOut.txt';

    public function __construct(Tracker $tracker)
    {
        $this->tracker = $tracker;
    }

    public function getCurrentCheckedOut($personId)
    {
        return (int) $this->tracker->read($personId, self::$currentlyCheckedOut);
    }

    public function getTotalCheckedOut($personId)
    {
        return (int) $this->tracker->read($personId, self::$totalCheckedOut);
    }

    public function addTotalCheckedOut($personId)
    {
        $currentlyTotalCheckedOut = (int) $this->tracker->read($personId, self::$totalCheckedOut);
        $this->tracker->write($personId, self::$totalCheckedOut, ++$currentlyTotalCheckedOut);
    }

    public function addCurrentCheckedOut($personId)
    {
        $currentlyCheckedOut = (int) $this->tracker->read($personId, self::$currentlyCheckedOut);
        $this->tracker->write($personId, self::$currentlyCheckedOut, ++$currentlyCheckedOut);
    }

    public function removeCurrentCheckoutOut($personId)
    {
        $currentlyCheckedOut = (int) $this->tracker->read($personId, self::$currentlyCheckedOut);
        $this->tracker->write($personId, self::$currentlyCheckedOut, --$currentlyCheckedOut);
    }
}
