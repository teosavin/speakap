<?php


namespace App\Entities;

use App\Interfaces\BookInterface;

class BookEntity implements BookInterface
{
    private $tracker;
    private static $checkInTime = 'checkInTime.txt';
    private static $checkOutTime = 'checkOutTime.txt';
    private static $isCheckedOutNow = 'isCheckedOutNow.txt';
    private static $longestCheckedOut = 'longestCheckedOut.txt';

    public function __construct(Tracker $tracker)
    {
        $this->tracker = $tracker;
    }

    public function setCheckIn(array $transaction)
    {
        $this->tracker->write($transaction['isbn'], self::$checkInTime, $transaction['timestamp']);
    }

    public function setCheckOut(array $transaction)
    {
        $this->tracker->write($transaction['isbn'], self::$checkOutTime, $transaction['timestamp']);
    }

    public function getCheckOut(string $isbn)
    {
        return $this->tracker->read($isbn, self::$checkOutTime);
    }

    public function getCheckIn(string $isbn)
    {
        return $this->tracker->read($isbn, self::$checkInTime);
    }

    public function getLongestCheckedOut(string $isbn)
    {
        return $this->tracker->read($isbn, self::$longestCheckedOut);
    }

    public function getIsCheckedOut(string $isbn)
    {
        return $this->tracker->read($isbn, self::$isCheckedOutNow);
    }

    public function setLongestCheckedOutByIsbn(string $isbn, string $value)
    {
        $this->tracker->write($isbn, self::$longestCheckedOut, $value);

        return true;
    }

    public function assertLongestCheckedOut(string $isbn, float $checkInTimestamp)
    {
        $checkOutTimestamp = $this->tracker->read($isbn, self::$checkOutTime);
        $longestCheckedOutTime = (float) $this->tracker->read($isbn, self::$longestCheckedOut);
        $currentCheckOutBookTime = (float) ($checkInTimestamp - $checkOutTimestamp);

        if ($currentCheckOutBookTime > $longestCheckedOutTime) {
            $this->setLongestCheckedOutByIsbn($isbn, (string) $currentCheckOutBookTime);
        }
    }

    public function setIsCheckedOutByIsbn(string $isbn, bool $checkedOut)
    {
        if ($checkedOut) {
            $this->tracker->write($isbn, self::$isCheckedOutNow, 'true');
        } else {
            $this->tracker->write($isbn, self::$isCheckedOutNow, 'false');
        }

        return true;
    }
}
