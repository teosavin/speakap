<?php


namespace App\Entities;

use DirectoryIterator;

class Tracker
{
    public static $basePath = __DIR__ . '/../../data/tmp/';

    public function __construct()
    {
        if (!file_exists(self::$basePath)) {
            if (!mkdir(self::$basePath, 0777, true)) {
                throw new \Exception('Unable to create directory ' . self::$basePath);
            }
        }
    }

    public function read($directory, $filename)
    {
        $path = self::$basePath . $directory . '/';

        if (!file_exists($path)) {
            return 0;
        }

        if (!file_exists($path . $filename)) {
            return 0;
        }

        $result = file_get_contents($path . $filename);
        if ($result === false) {
            throw new \Exception("Unable to read file: {$filename}");
        }

        return $result;
    }

    public function write($directory, $filename, $value)
    {
        $path = self::$basePath . $directory . '/';

        if (!file_exists($path) && !file_exists($path . $filename)) {
            if (!mkdir($path, 0777, true)) {
                throw new \Exception('Unable to create directory ' . $path);
            }
        }

        $result = file_put_contents($path . $filename, $value);
        if ($result === false) {
            throw new \Exception("Unable to write to the file {$path}{$filename}. Value: {$value}");
        }
        return $result;
    }

    public static function cleanUp()
    {
        if (file_exists(self::$basePath)) {
            self::deleteFolder(self::$basePath);
        }
    }

    private static function deleteFolder($path)
    {
        foreach (new DirectoryIterator($path) as $file) {
            if ($file->isDot()) {
                continue;
            }
            if ($file->isFile()) {
                unlink($file->getPathName());
            }
            if ($file->isDir()) {
                self::deleteFolder($file->getPathName());
            }
        }
        rmdir($path);
    }
}
