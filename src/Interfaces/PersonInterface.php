<?php


namespace App\Interfaces;

interface PersonInterface
{
    public function getTotalCheckedOut($personId);
    public function getCurrentCheckedOut($personId);

    public function addTotalCheckedOut($personId);
    public function addCurrentCheckedOut($personId);
    public function removeCurrentCheckoutOut($personId);
}
