<?php

namespace App\Interfaces;

interface ImporterFactoryInterface
{
    public function createImporter($type): ImporterInterface;
}
