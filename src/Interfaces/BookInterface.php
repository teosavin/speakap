<?php


namespace App\Interfaces;

interface BookInterface
{
    public function setCheckOut(array $transaction);
    public function setCheckIn(array $transaction);
    public function assertLongestCheckedOut(string $isbn, float $checkedInTimestamp);

    public function getCheckOut(string $isbn);
    public function getCheckIn(string $isbn);
    public function getLongestCheckedOut(string $isbn);
    public function getIsCheckedOut(string $isbn);

    public function setLongestCheckedOutByIsbn(string $isbn, string $value);
    public function setIsCheckedOutByIsbn(string $isbn, bool $checkedOut);
}
