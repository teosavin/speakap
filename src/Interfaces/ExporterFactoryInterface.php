<?php

namespace App\Interfaces;

interface ExporterFactoryInterface
{
    public function createExporter($type): ExporterInterface;
}
