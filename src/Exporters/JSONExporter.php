<?php


namespace App\Exporters;

class JSONExporter extends BaseExporter
{
    public function createContents()
    {
        return json_encode([
            'Which person has the most checkouts (which person_id)' => $this->personWithMostBooksCheckedOut,
            'Which book was checked out the longest time in total (summed up over all transactions)' => $this->longestBookCheckedOut,
            'How many books are checked out at this moment' => $this->mostBooksCheckedOutNow,
            'Who currently has the largest number of books' => $this->currentAmountOfCheckedOutBooks
        ]);
    }
}
