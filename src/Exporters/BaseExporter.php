<?php


namespace App\Exporters;

use App\Entities\Tracker;
use App\Interfaces\BookInterface;
use App\Interfaces\ExporterInterface;
use App\Interfaces\PersonInterface;

abstract class BaseExporter implements ExporterInterface
{
    private $bookEntity;
    private $personEntity;
    protected $mostBooksCheckedOut;
    protected $personWithMostBooksCheckedOut;

    protected $mostBooksCheckedOutNow;
    protected $personWithMostBooksCheckedOutNow;

    protected $longestBookCheckedOut;
    protected $timeOfLongestBookCheckedOut;

    protected $currentAmountOfCheckedOutBooks = 0;

    public static $outputPath = __DIR__ . '/../../data/output/';


    public function __construct(BookInterface $bookEntity, PersonInterface $personEntity)
    {
        $this->bookEntity = $bookEntity;
        $this->personEntity = $personEntity;

        if (!file_exists(self::$outputPath)) {
            if (!mkdir(self::$outputPath, 0777, true)) {
                throw new \Exception('Unable to create directory ' . self::$outputPath);
            }
        } else {
            if (!is_writable(self::$outputPath)) {
                chmod(self::$outputPath, 0777);
            }
        }
    }

    abstract public function createContents();

    public function export($file)
    {
        $path = BaseExporter::$outputPath . $file;
        $this->iterateDirectories();
        file_put_contents($path, $this->createContents());
    }

    public function iterateDirectories()
    {
        foreach (new \DirectoryIterator(Tracker::$basePath) as $directory) {
            if (is_numeric($directory->getFilename())) {
                // person directory
                $this->assertMaxTotalCheckedOut($directory->getFilename());
                $this->assertMaxCheckedOut($directory->getFilename());
            } elseif (!$directory->isDot()) {
                // book directory
                $this->assertLongestBookCheckedOut($directory->getFilename());
                $this->assertCheckedOut($directory->getFilename());
            }
        }
    }

    protected function assertMaxTotalCheckedOut($personId)
    {
        $currentTotalCheckedOut = (int) $this->personEntity->getTotalCheckedOut($personId);
        if ($currentTotalCheckedOut > $this->mostBooksCheckedOut) {
            $this->mostBooksCheckedOut = $currentTotalCheckedOut;
            $this->personWithMostBooksCheckedOut = $personId;
        }
    }

    protected function assertMaxCheckedOut($personId)
    {
        $currentCheckedOut = (int) $this->personEntity->getCurrentCheckedOut($personId);
        if ($currentCheckedOut > $this->personWithMostBooksCheckedOutNow) {
            $this->mostBooksCheckedOutNow = $currentCheckedOut;
            $this->personWithMostBooksCheckedOutNow = $personId;
        }
    }

    protected function assertLongestBookCheckedOut($isbn)
    {
        $currentLongestTime = (int) $this->bookEntity->getLongestCheckedOut($isbn);
        if ($currentLongestTime > $this->timeOfLongestBookCheckedOut) {
            $this->timeOfLongestBookCheckedOut = $currentLongestTime;
            $this->longestBookCheckedOut = $isbn;
        }
    }

    protected function assertCheckedOut($isbn)
    {
        if ('true' === $this->bookEntity->getIsCheckedOut($isbn)) {
            $this->currentAmountOfCheckedOutBooks++;
        }
    }
}
