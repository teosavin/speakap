<?php


namespace App\Exporters;

class TxtExporter extends BaseExporter
{
    public function createContents()
    {
        return "Which person has the most checkouts (which person_id): {$this->personWithMostBooksCheckedOut}\n
Which book was checked out the longest time in total (summed up over all transactions): {$this->longestBookCheckedOut}\n
How many books are checked out at this moment: {$this->mostBooksCheckedOutNow}\n
Who currently has the largest number of books: {$this->currentAmountOfCheckedOutBooks}\n
";
    }
}
