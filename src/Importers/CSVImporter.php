<?php


namespace App\Importers;

use DateTime;

class CSVImporter extends BaseImporter
{
    private $headers;

    public function import($file)
    {
        $rowId = 1;

        if (($handle = fopen($file, "r")) !== false) {
            while (($transaction = fgetcsv($handle, 1000, ",")) !== false) {
                // header of the file
                if ($rowId === 1) {
                    $this->headers = $this->getHeaders($transaction);
                    $this->assertHeaders($this->headers);

                    $rowId++;
                    continue;
                }

                $transaction = $this->getTransaction($transaction);

                if ($transaction['action'] === 'check-in') {
                    $this->updateCheckIn($transaction);
                } elseif ($transaction['action'] === 'check-out') {
                    $this->updateCheckOut($transaction);
                } else {
                    throw new \Exception("Action is not defined in csv file {$file} on row {$rowId}");
                }

                $rowId++;
            }
            fclose($handle);
        }
    }

    public function getHeaders(array $headers)
    {
        foreach ($headers as $key => $headerField) {
            $headers[$key] = trim(strtolower($headerField));
        }
        return $headers;
    }

    public function assertHeaders()
    {
        if (!is_array($this->headers)) {
            throw new \Exception('Headers not set');
        }

        foreach (self::$headerFields as $headerField) {
            if (!in_array($headerField, $this->headers)) {
                throw new \Exception("Header field '{$headerField}' not found");
            }
        }

        return true;
    }

    // TODO assert that all are filled
    public function getTransaction(array $row)
    {
        $transaction = [];
        foreach ($this->headers as $key => $headerField) {
            $row[$key] = strtolower(trim($row[$key]));
            switch ($headerField) {
                case 'timestamp':
                    $transaction['timestamp'] = DateTime::createFromFormat(DateTime::W3C, strtoupper($row[$key]))->getTimestamp();
                    break;
                case 'person':
                    $transaction['person'] = (int) $row[$key];
                    break;
                case 'isbn':
                    $transaction['isbn'] = (string) strtolower(trim($row[$key]));
                    break;
                case 'action':
                    $transaction['action'] = (string) strtolower(trim($row[$key]));
                    break;
            }
        }

        return $transaction;
    }
}
