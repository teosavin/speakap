<?php

namespace App\Importers;

use App\Entities\PersonEntity;
use App\Interfaces\BookInterface;
use App\Interfaces\ImporterInterface;

abstract class BaseImporter implements ImporterInterface
{
    protected static $headerFields = [
        'person',
        'timestamp',
        'isbn',
        'action'
    ];
    protected $bookEntity;
    protected $personEntity;

    abstract public function import($file);

    public function __construct(BookInterface $bookEntity, PersonEntity $personEntity)
    {
        $this->bookEntity = $bookEntity;
        $this->personEntity = $personEntity;
    }

    protected function updateCheckIn(array $transaction)
    {
        $this->bookEntity->setIsCheckedOutByIsbn($transaction['isbn'], false);
        $this->bookEntity->setCheckIn($transaction);
        $this->bookEntity->assertLongestCheckedOut($transaction['isbn'], $transaction['timestamp']);

        $this->personEntity->removeCurrentCheckoutOut($transaction['person']);
    }

    protected function updateCheckOut(array $transaction)
    {
        $this->bookEntity->setIsCheckedOutByIsbn($transaction['isbn'], true);
        $this->bookEntity->setCheckOut($transaction);

        $this->personEntity->addCurrentCheckedOut($transaction['person']);
        $this->personEntity->addTotalCheckedOut($transaction['person']);
    }
}
