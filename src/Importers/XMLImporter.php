<?php


namespace App\Importers;

use DateTime;
use DOMDocument;
use XMLReader;

class XMLImporter extends BaseImporter
{
    public function import($file)
    {
        $reader = new XMLReader;
        $reader->open($file);
        $doc = new DOMDocument;

        // move to the first <record /> node
        while ($reader->read() && $reader->name !== 'record') {
        }

        while ($reader->name === 'record') {
            $record = simplexml_import_dom($doc->importNode($reader->expand(), true));

            $transaction = $this->getTransaction($record);

            if ($transaction['action'] === 'check-in') {
                $this->updateCheckIn($transaction);
            } elseif ($transaction['action'] === 'check-out') {
                $this->updateCheckOut($transaction);
            }

            // go to the next node
            $reader->next('record');
        }
    }

    public function getHeaders($headers)
    {
        foreach ($headers as $key => $headerField) {
            $headers[$key] = trim(strtolower($headerField));
        }
        return $headers;
    }

    public function getTransaction(\SimpleXMLElement $record)
    {
        $transaction = [];
        $transaction['person'] = (int) $this->getAttribute('id', $record->person);
        $transaction['isbn'] = (string) $record->isbn;
        $transaction['timestamp'] = DateTime::createFromFormat(DateTime::W3C, $record->timestamp)->getTimestamp();
        $transaction['action'] = (string) $this->getAttribute('type', $record->action);
        return $transaction;
    }

    private function getAttribute($attribute, $object)
    {
        if (isset($object[$attribute])) {
            return (string) $object[$attribute];
        }
    }
}
