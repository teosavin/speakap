<?php


namespace App;

use App\Entities\BookEntity;
use App\Entities\PersonEntity;
use App\Entities\Tracker;
use App\Factories\ExporterFactory;
use App\Factories\ImporterFactory;

class Report
{
    public function import(array $files)
    {
        foreach ($files as $file) {
            if (!file_exists($file)) {
                throw new \Exception("Input file {$file} not found");
                die();
            }
            $extension = pathinfo($file, PATHINFO_EXTENSION);
            $importer = new ImporterFactory(new BookEntity(new Tracker()), new PersonEntity(new Tracker()));
            $importer = $importer->createImporter($extension);
            $importer->import($file);
        }
    }

    public function export(string $file)
    {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $exporter = new ExporterFactory(new BookEntity(new Tracker()), new PersonEntity(new Tracker()));
        $exporter = $exporter->createExporter($extension);
        $exporter->export($file);
    }

    public function __destruct()
    {
        Tracker::cleanUp();
    }
}
