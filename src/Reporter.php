<?php

namespace App;

// TODO: refactor this masterpiece.

// status
define("CHECKIN", 1);
define("CHECKOUT", 2);

// XML format
define('WEBFORMAT', DateTime::W3C);


// fields
define('USER_ID', 0);
define('BOOK_ID', USER_ID + 1);
define('TRANSACTION_DATE', BOOK_ID + 1);
define('OPERATION_TYPE', TRANSACTION_DATE + 1);

// Reporter class
class Reporter
{
    var $data;
    var $data2;

    // construct
    function __construct($import_file_paths)
    {
        $this->data['most_active_reader'] = null;
        $this->data['book_longest_time_in_total'] = null;
        $this->data['checked_out_count'] = 0;
        $this->data['user_with_biggest_amount_of_books'] = null;

        // to make it the same as a main data
        $this->data2 = array();

        $this->data2['readers_information'] = array();
        $this->data2['long_books_information'] = array();
        $this->data2['how_many_books_information'] = array();
        $this->data2['general_information'] = array();
        $this->data2['ouns_more_than_others_information'] = array();



        $recordsInAllFiles = array(); // all records in an all files
        $all_counter = 0;

        for ($i = 0; $i < count($import_file_paths); $i++) {
            $x = @simplexml_load_file($import_file_paths[$i]);

            if (!$x) {
                die('cannot open the file');
            }

            for ($j = 0; $j < $x->record->count(); $j++) {
                $user_eid = intval((string)$x->record[$j]->person['id']);
                $ISBN     = ((string)$x->record[$j]->isbn);
                $date     =  DateTime::createFromFormat(WEBFORMAT, (string)$x->record[$j]->timestamp);
                $type     = (strlen((string)$x->record[$j]->action["type"]) > 8) ? CHECKOUT : CHECKIN;

                $recordsInAllFiles[$all_counter++] = array(
                    $user_eid, $ISBN, $date, $type
                );
            }
        }

        foreach ($recordsInAllFiles as $recordInAllFiles) {
            if ($recordInAllFiles[OPERATION_TYPE] == CHECKOUT) {
                if (key_exists($recordInAllFiles[USER_ID], $this->data2['readers_information'])) {
                    $this->data2['readers_information'][$recordInAllFiles[USER_ID]] ++;
                } else {
                    $this->data2['readers_information'][$recordInAllFiles[USER_ID]] = 1;
                }
            }
        }

        $user = 0;
        $count = 0;
        foreach ($this->data2['readers_information'] as $k => $v) {
            //          if ($v >= $count) {
            if ($v > $count) {
                $count = $v;
                $user = $k;
            }
        }

        $this->data['most_active_reader'] = $user;


        foreach ($recordsInAllFiles as $recordInAllFiles) {
            if ($recordInAllFiles[OPERATION_TYPE] == CHECKOUT) {
                $this->data2["book_longest_time_in_total"]
                [ $recordInAllFiles[USER_ID] . ":" . $recordInAllFiles[BOOK_ID] ]
                ['begin'] = $recordInAllFiles[TRANSACTION_DATE];
            } elseif ($recordInAllFiles[OPERATION_TYPE] == CHECKIN) {
                $this->data2["book_longest_time_in_total"]
                [ $recordInAllFiles[USER_ID] . ":" . $recordInAllFiles[BOOK_ID] ]
                ['end'] = $recordInAllFiles[TRANSACTION_DATE];
            }
        }


        $books = [];

        foreach ($this->data2["book_longest_time_in_total"] as $key => $diapazon) {
            if (key_exists('end', $diapazon) && key_exists('begin', $diapazon)) {
                $bookId = explode(':', $key)[1];
                if (array_key_exists($bookId, $books)) {
                    $books[$bookId] += ($diapazon['end']->getTimestamp() - $diapazon['begin']->getTimestamp());
                } else {
                    $books[$bookId] = ($diapazon['end']->getTimestamp() - $diapazon['begin']->getTimestamp());
                }
            }
        }

        $book = 0;
        $how_long = 0;
        foreach ($books as $k => $v) {
            if ($how_long < $v) {
                $how_long = $v;
                $book = $k;
            }
        }

        $this->data['book_longest_time_in_total']  = $book;


        foreach ($recordsInAllFiles as $recordInAllFiles) {
            if ($recordInAllFiles[OPERATION_TYPE] == CHECKOUT) {
                $this->data2["how_many_books_information"]
                [ $recordInAllFiles[USER_ID] . ":" . $recordInAllFiles[BOOK_ID] ] = "IT EXIST!";
            } elseif ($recordInAllFiles[OPERATION_TYPE] == CHECKIN) {
                unset($this->data2["how_many_books_information"][ $recordInAllFiles[USER_ID] . ":" . $recordInAllFiles[BOOK_ID] ]);
            }
        }

        $this->data['checked_out_count'] = count($this->data2["how_many_books_information"]);


        foreach ($recordsInAllFiles as $recordInAllFiles) {
            if ($recordInAllFiles[OPERATION_TYPE] == CHECKOUT) {
                $this->data2["ouns_more_than_others_information"]
                [ $recordInAllFiles[USER_ID] . ":" . $recordInAllFiles[BOOK_ID] ] = "he OWN!";
            } elseif ($recordInAllFiles[OPERATION_TYPE] == CHECKIN) {
                unset($this->data2["ouns_more_than_others_information"][ $recordInAllFiles[USER_ID] . ":" . $recordInAllFiles[BOOK_ID] ]);
            }
        }


        $users = array(); //array of users and book counts
        foreach ($this->data2["ouns_more_than_others_information"] as $k => $v) {
            $user2 = explode(':', $k)[0];

            if (!key_exists($user2, $users)) {
                $users[$user2] = 0;
            }
            $users[$user2]++;
        }

        $user_having_books = null;
        $how_mony_books = 0;

        foreach ($users as $k => $v) {
            if ($how_mony_books < $v) {
                $how_mony_books = $v;
                $user_having_books = $k;
            }
        }

         $this->data['user_with_biggest_amount_of_books'] = $user_having_books;
    }

    function report($reportFilePath)
    {
        file_put_contents(
            $reportFilePath,
            "Which person has the most checkouts (which person_id): {$this->data['most_active_reader']}\n
Which book was checked out the longest time in total (summed up over all transactions): {$this->data['book_longest_time_in_total']}\n
How many books are checked out at this moment: {$this->data['checked_out_count']}\n
Who currently has the largest number of books: {$this->data['user_with_biggest_amount_of_books']}\n
"
        );
    }
}
