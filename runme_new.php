<?php

use App\Report;

require __DIR__ . '/vendor/autoload.php';
$reporter = new Report();

$reporter->import([
    __DIR__ . '/data/input/2017-01.xml',
    __DIR__ . '/data/input/2017-02.csv'
]);

$reporter->export('report.json');
$reporter->export('report2.txt');
